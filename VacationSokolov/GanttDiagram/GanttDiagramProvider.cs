﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VacationSokolov.GranttDiagram
{
    public class GanttDiagramProvider
    {
        private DataGridView _granttDiagram;

        public GanttDiagramProvider(DataGridView granttDiagram)
        {
            _granttDiagram = granttDiagram;
        }

        /// <summary>
        /// main method for adding a employee to a diagram
        /// </summary>
        /// <param name="employeeName">employee name,surname</param>
        /// <param name="fromDate">vacation start date</param>
        /// <param name="toDate">vacation end date</param>
        public void AddEmployee(string employeeName, DateTime fromDate, DateTime toDate)
        {
            var lastColumn = DateTime.Parse(_granttDiagram.Columns[_granttDiagram.Columns.Count - 1].HeaderText);

            
            CheckColumns(lastColumn, toDate);

            _granttDiagram.Rows.Add(employeeName);

            //get row id for employee
            var employeeId = _granttDiagram.Rows.Count - 2;

            //рисуем
            for (var i = FindColumnIdByDate(fromDate); i <= FindColumnIdByDate(toDate); i++)
            {
                _granttDiagram.Rows[employeeId].Cells[i + 1].Style.BackColor = Color.LightSkyBlue;
                for (var j = 0; j < _granttDiagram.Rows.Count;j++)
                {
                    if ((_granttDiagram.Rows[j].Cells[i + 1].Style.BackColor != Color.LightSkyBlue &&
                         _granttDiagram.Rows[j].Cells[i + 1].Style.BackColor != Color.MediumSlateBlue) ||
                        j == employeeId) continue;

                    _granttDiagram.Rows[j].Cells[i + 1].Style.BackColor = Color.MediumSlateBlue;
                    _granttDiagram.Rows[employeeId].Cells[i + 1].Style.BackColor = Color.MediumSlateBlue;
                }
            }
        }

        private void CheckColumns(DateTime lastColumn, DateTime toDate)
        {
            if (lastColumn < toDate)
            {
                AddMissingColumns(lastColumn, toDate);
            }
        }

        private void AddMissingColumns(DateTime start, DateTime end)
        {
            for (var i = 1; i < (end - start).Days + 1; i++)
            {
                var tempDate = start.AddDays(i);
                _granttDiagram.Columns.Add("", $"{tempDate.Year}.{tempDate.Month}.{tempDate.Day}");
            }
        }

        private int FindColumnIdByDate(DateTime date)
        {
            //date for 1 column
            var firstColumn = DateTime.Parse(_granttDiagram.Columns[1].HeaderText);

            return (date - firstColumn).Days;
        }
    }
}
