﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin.Controls;
using VacationSokolov.GranttDiagram;
using Zeroit.Framework.MaterialDesign.Controls;

namespace VacationSokolov
{
    public partial class Form1 : MaterialForm
    {

        public Form1()
        {
            InitializeComponent();

            GanttDiagram.ReadOnly = true;
            //добавляем первые колонки
            GanttDiagram.Columns.Add("", "Имя сотрудника");
            GanttDiagram.Columns.Add("", $"{DateTime.Now.Year}.{DateTime.Now.Month}.{DateTime.Now.Day}");
        }

        /// <summary>
        /// event, on button click
        /// </summary>
        private void AddButton_Click(object sender, EventArgs e)
        {
            var name = NameEmployee.Text;
            var startVacation = DateTime.Parse(StartVacation.Text);
            var finishVacation = DateTime.Parse(FinishVacation.Text);

            //проверка на введенные данные
            if (finishVacation <= startVacation || startVacation.Date < DateTime.Now.Date
                                                || (!Regex.IsMatch(name, "^[A-Я][а-я]+\\s[A-Я][а-я]+\\s[A-Я][а-я]+$")
                                                && !Regex.IsMatch(name, "^[A-Z][a-z]+\\s[A-Z][a-z]+\\s[A-Z][a-z]+$")))
            {
                MessageBox.Show("error");
                return;
            }

            //инстанцируем класс GanttDiagramProvider
            //TODO: сделай свой элемент вместо таблицы
            var gDiagram = new GanttDiagramProvider(GanttDiagram);

            //добавляем сотрудника в диаграмму ганта
            gDiagram.AddEmployee(name, startVacation, finishVacation);
        }
    }
}
