﻿namespace VacationSokolov
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddButton = new System.Windows.Forms.Button();
            this.StartVacation = new System.Windows.Forms.DateTimePicker();
            this.FinishVacation = new System.Windows.Forms.DateTimePicker();
            this.NameEmployee = new System.Windows.Forms.TextBox();
            this.GanttDiagram = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GanttDiagram)).BeginInit();
            this.SuspendLayout();
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(371, 125);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(75, 23);
            this.AddButton.TabIndex = 0;
            this.AddButton.Text = "Добавить\r\n";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // StartVacation
            // 
            this.StartVacation.Location = new System.Drawing.Point(317, 99);
            this.StartVacation.Name = "StartVacation";
            this.StartVacation.Size = new System.Drawing.Size(200, 20);
            this.StartVacation.TabIndex = 1;
            // 
            // FinishVacation
            // 
            this.FinishVacation.Location = new System.Drawing.Point(588, 99);
            this.FinishVacation.Name = "FinishVacation";
            this.FinishVacation.Size = new System.Drawing.Size(200, 20);
            this.FinishVacation.TabIndex = 2;
            // 
            // NameEmployee
            // 
            this.NameEmployee.Location = new System.Drawing.Point(12, 99);
            this.NameEmployee.Name = "NameEmployee";
            this.NameEmployee.Size = new System.Drawing.Size(200, 20);
            this.NameEmployee.TabIndex = 3;
            // 
            // GanttDiagram
            // 
            this.GanttDiagram.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GanttDiagram.Location = new System.Drawing.Point(12, 154);
            this.GanttDiagram.Name = "GanttDiagram";
            this.GanttDiagram.Size = new System.Drawing.Size(776, 284);
            this.GanttDiagram.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "ФИО сотрудника";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(354, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Дата начала отпуска";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(620, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Дата окончания отпуска";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GanttDiagram);
            this.Controls.Add(this.NameEmployee);
            this.Controls.Add(this.FinishVacation);
            this.Controls.Add(this.StartVacation);
            this.Controls.Add(this.AddButton);
            this.Name = "Form1";
            this.Text = "Отпуска сотрудников";
            ((System.ComponentModel.ISupportInitialize)(this.GanttDiagram)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.DateTimePicker StartVacation;
        private System.Windows.Forms.DateTimePicker FinishVacation;
        private System.Windows.Forms.TextBox NameEmployee;
        private System.Windows.Forms.DataGridView GanttDiagram;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

